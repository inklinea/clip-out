# Clip Out

Inkscape Extension to clip a background image by object outlines then export to multiple png. 

Supports: 

Exporting individual clipped objects ( normal and inverse )

Export master of all clipped objects ( normal and inverse )

Cropping to resulting image size, or exporting to background image size.


Appears under Extensions>Export

Requires Inkscape 1.1+ -->
The image to be clipped must be the last selected.
An easy way to do this, is select all then shift &amp; left click the image twice to make it the last selected.
It does require that you have saved our svg file
at least once before using ( will not work on an unsaved svg )
