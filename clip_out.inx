<?xml version="1.0" encoding="UTF-8"?>

<!--Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]-->

<!--        This program is free software; you can redistribute it and/or modify-->
<!--        it under the terms of the GNU General Public License as published by-->
<!--        the Free Software Foundation; either version 2 of the License, or-->
<!--        (at your option) any later version.-->

<!--        This program is distributed in the hope that it will be useful,-->
<!--        but WITHOUT ANY WARRANTY; without even the implied warranty of-->
<!--        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the-->
<!--        GNU General Public License for more details.-->

<!--        You should have received a copy of the GNU General Public License-->
<!--        along with this program; if not, write to the Free Software-->
<!--        Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.-->


<!--        #############################################################################-->
<!--        #  Quick Export - Quickly export Inkscape svg, plain svg, and png-->
<!--        #  After setting the options in the main dialogue-->
<!--        #  Assign a shortcut in Inkscape Edit>Preferences>Interface>Keyboard to org.inkscape.inklinea.quick_export.noprefs-->
<!--        #  For shortcut triggered quick export-->
<!--        # Requires Inkscape 1.1+ -->
<!--        #############################################################################-->

<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
    <name>Clip Out</name>
    <id>org.inkscape.inklinea.clip_out</id>

    <param name="notebook_main" type="notebook">
        <page name="settings_page" gui-text="Settings">
            <hbox>
            <param name="clip_type_inverse" type="boolean" gui-text="Inverse" gui-description="Make holes :)">false</param>
            <vbox>
            <param name="shape_unit_fix" type="boolean" gui-text="Shape Unit Fix" gui-description="Unit correction for rect / ellipse / circle">true</param>
            </vbox>

            </hbox>
            <separator></separator>
            <separator></separator>
            <hbox>
            <param name="output_set" type="optiongroup" appearance="radio" gui-text="Output">
                <option value="master_only" gui-description="Export Single Combined Clip">Master Only</option>
                <option value="separate" gui-description="Export Each Object as Separate Clips">Separate</option>
                <option value="master_and_separate" gui-description="Export Both Single Combined Clips and Separate Clips">Master &amp; Separate</option>
            </param>
            <vbox>
                <param name="canvas_to_selection" type="optiongroup" appearance="radio" gui-text="Resize to Selection" gui-description="Crop Canvas To Resulting Clip">
                    <option value="true">Yes</option>
                    <option value="false">No</option>
                </param>

            </vbox>
            </hbox>
            <param name="png_dpi" type="int" min="10" max="99999999" gui-text="PNG dpi">96</param>
            <separator/>
            <param type="path" name="save_path" gui-text="File Save Path" mode="folder">None Selected</param>

         </page>
        <page name="about_page" gui-text="About">


    <label xml:space="preserve">
Clip Out - Export multiple clipped png images using paths / shapes and a background image
   </label>
            <label appearance="url">
                https://gitlab.com/inklinea/quick-export
            </label>
            <label appearance="url">
                https://inkscape.org/~inklinea/
            </label>
            <label xml:space="preserve">
Requires Inkscape 1.1+ -->
The image to be clipped must be the last selected.
An easy way to do this, is select all then shift &amp; left click the image twice to make it the last selected.
It does require that you have saved our svg file
at least once before using ( will not work on an unsaved svg )
            </label>
        </page>
    </param>
    <effect  needs-live-preview="false">
        <object-type>path</object-type>
        <effects-menu>
            <submenu name="Export"/>
        </effects-menu>
    </effect>
    <script>
        <command location="inx" interpreter="python">clip_out.py</command>
    </script>
</inkscape-extension>
